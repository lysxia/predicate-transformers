Definition wpPartial {A B} (f : A -> option B) (P : A -> B -> Prop) (a : A) : Prop :=
  match f a with
  | None => False
  | Some b => P a b
  end.
