From Coq Require Import
  Setoid Relations Morphisms.

(* Predicates *)

Definition eq_pred {A} (p q : A -> Prop) : Prop :=
  forall a, p a <-> q a.

Instance Equivalence_eq_pred {A} : Equivalence (@eq_pred A).
Proof. firstorder. Qed.

(* Relations *)

Definition eq_rel {A B} (RL RR : A -> B -> Prop) : Prop :=
  forall a b, RL a b <-> RR a b.

Instance Equivalence_eq_rel {A B} : Equivalence (@eq_rel A B).
Proof.
  constructor; try firstorder.
  cbv; intros.
  do 2
    match goal with
    | [ H : _ |- _ ] => edestruct H; clear H
    end; auto.
Qed.

Definition le_rel {A B} (RL RR : A -> B -> Prop) : Prop :=
  forall a b, RL a b -> RR a b.

Instance sub_eq_le_rel {A B}
  : subrelation (@eq_rel A B) le_rel.
Proof. firstorder. Qed.

Instance Transitive_le_rel {A B} : Transitive (@le_rel A B).
Proof. firstorder. Qed.

Definition cat_rel {A B C} (RAB : A -> B -> Prop) (RBC : B -> C -> Prop) (a : A) (c : C) : Prop :=
  exists b, RAB a b /\ RBC b c.

Instance Proper_cat_rel {A B C}
  : Proper (eq_rel ==> eq_rel ==> eq_rel)
           (@cat_rel A B C).
Proof.
  intros ? ? H1 ? ? H2.
  red in H1, H2.
  split;
    intros []; eexists;
    [ rewrite <- H1, <- H2 | rewrite H1, H2 ];
    eassumption.
Qed.  

(* Functor from functions to relations. *)
Definition fun_rel {A B} (f : A -> B) (a : A) : B -> Prop :=
  eq (f a).

Lemma cat_eq_l {A B} (R : A -> B -> Prop)
  : eq_rel (cat_rel eq R) R.
Proof. firstorder (subst; auto). Qed.

Lemma cat_eq_r {A B} (R : A -> B -> Prop)
  : eq_rel (cat_rel R eq) R.
Proof. firstorder (subst; auto). Qed.

Lemma cat_cat {A B C D}
      (R1 : A -> B -> Prop) (R2 : B -> C -> Prop) (R3 : C -> D -> Prop)
  : eq_rel (cat_rel (cat_rel R1 R2) R3) (cat_rel R1 (cat_rel R2 R3)).
Proof. firstorder. Qed.

(* Combining predicates and relations *)

Definition rmap_pred {A B} (R : A -> B -> Prop) (P : A -> Prop) (b : B) : Prop :=
  exists a, P a /\ R a b.

Lemma rmap_rel {A B C} (RL : A -> B -> Prop) (RR : B -> C -> Prop) (a : A)
  : eq_pred (rmap_pred RR (RL a)) ((cat_rel RL RR) a).
Proof. firstorder. Qed.

Lemma eq_rel_pred {A B} (R1 R2 : A -> B -> Prop)
  : eq_rel R1 R2 <-> forall a, eq_pred (R1 a) (R2 a).
Proof. firstorder. Qed.

Instance Proper_rmap_pred {A B}
  : Proper (eq_rel ==> eq_pred ==> eq_pred)
           (@rmap_pred A B).
Proof. firstorder. Qed.

Lemma rmap_pred_eq {A} (P : A -> Prop) : eq_pred (rmap_pred eq P) P.
Proof. firstorder (subst; auto). Qed.

Lemma compose_rmap {A B C} (R1 : A -> B -> Prop) (R2 : B -> C -> Prop)
  : eq_rel (fun a => rmap_pred R2 (R1 a)) (cat_rel R1 R2).
Proof. firstorder. Qed.

Lemma rmap_rmap {A B C} (R1 : A -> B -> Prop) (R2 : B -> C -> Prop) (P : A -> Prop)
  : eq_pred (rmap_pred R2 (rmap_pred R1 P)) (rmap_pred (cat_rel R1 R2) P).
Proof. firstorder. Qed.

Lemma eq_pred_fun_rel {A B} (f : A -> B) (a : A)
  : eq_pred (eq (f a)) (fun_rel f a).
Proof. reflexivity. Qed.

Module RelNotations_.

Declare Scope rel_scope.
Delimit Scope rel_scope with rel.

Infix "=p" := eq_pred (at level 80) : rel_scope.
Infix "=r" := eq_rel (at level 80) : rel_scope.
Infix "<=r" := le_rel (at level 80) : rel_scope.
Infix "><" := cat_rel (at level 41, right associativity) : rel_scope.
Infix "<|" := rmap_pred (at level 41, right associativity) : rel_scope.

End RelNotations_.

Module RelNotations.
Export RelNotations_.
Open Scope rel_scope.
End RelNotations.
