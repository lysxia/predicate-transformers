(* Example from:
     A Predicate Transformer Semantics for Effects.
     Wouter Swierstra, Tim Bannen. ICFP 2019. *)

From Coq Require Import Arith.
From PT Require Import
  Relations Monad WP Predicate.

Import RelNotations.

Inductive expr : Type :=
| Val : nat -> expr
| Div : expr -> expr -> expr
.

Inductive eval : expr -> nat -> Prop :=
| EvalVal : forall (n : nat), eval (Val n) n
| EvalDiv : forall (e1 e2 : expr) (v1 v2 : nat),
    eval e1 v1 ->
    eval e2 (S v2) ->
    eval (Div e1 e2) (Nat.div v1 (S v2))
.

Definition div_option (a b : nat) : option nat :=
  match b with
  | O => None
  | S _ => Some (Nat.div a b)
  end.

Notation bind' u k := (subst k u).

Fixpoint interp (e : expr) : option nat :=
  match e with
  | Val n => Some n
  | Div e1 e2 =>
    bind' (interp e1) (fun v1 =>
    bind' (interp e2) (fun v2 =>
    div_option v1 v2))
  end.

Fixpoint safe_div (e : expr) : Prop :=
  match e with
  | Val _ => True
  | Div e1 e2 =>
    safe_div e1 /\
    safe_div e2 /\
    ~ eval e2 O
  end.

Opaque Nat.div.

Theorem correct_interp (e : expr) : safe_div e -> wpPartial interp eval e.
Proof.
  induction e.
  - cbn. constructor.
  - cbn. intros ( ? & ? & ? ).
    unfold wpPartial in *; cbn.
    destruct (interp e1); try contradiction.
    destruct (interp e2); try contradiction.
    destruct n0; [ exfalso; auto | ].
    cbn.
    constructor; auto.
Qed.

Definition isSome {A} (P : A -> Prop) : option A -> Prop :=
  rmap_pred (fun_rel Some) P.

Lemma subst_isSome {A B} (P : A -> Prop) (Q : option B -> Prop) (k : A -> option B -> Prop)
  : (forall n, P n -> k n =p Q) -> subst_pred k (isSome P) =p Q.
Proof.
Admitted.

Definition imp_pred {A} (P Q : A -> Prop) : Prop :=
  forall a, P a -> Q a.

Infix "->p" := imp_pred (at level 80).

Lemma Some_isSome {A} (P : A -> Prop) (a : A) : P a -> lift_pred (Some a) ->p isSome P.
Proof. firstorder. Qed.

Lemma isSome_Some {A} (P : A -> Prop) (a : A) : (forall a0, P a0 -> a0 = a) -> isSome P ->p lift_pred (Some a).
Proof. cbv; firstorder. apply H in H0; subst; auto. Qed.

Lemma Some_isSome_eq {A} (P : A -> Prop) (a : A)
  : P a -> (forall a0, P a0 -> a0 = a) -> lift_pred (Some a) =p isSome P.
Proof.
Admitted.

Lemma eval_injective e n
  : eval e n -> forall n0, eval e n0 -> n0 = n.
Proof.
Admitted.

Lemma Some_isSome_eval e n
  : eval e n -> lift_pred (Some n) =p isSome (eval e).
Proof.
  intros; apply Some_isSome_eq; auto.
  apply eval_injective; auto.
Qed.

Theorem correct_interp' (e : expr)
  : safe_div e -> (lift_pred (interp e) =p isSome (eval e)).
Proof.
  induction e; cbn; intros.
  - apply Some_isSome_eval; constructor.
  - destruct H as (? & ? & ?).
    rewrite lift_subst, IHe1; auto.
    apply subst_isSome; intros.
    rewrite lift_subst, IHe2; auto.
    apply subst_isSome; intros.
    destruct n0; try contradiction; cbn.
    apply Some_isSome_eval; constructor; auto.
Qed.
