From Coq Require Import
     Setoid Morphisms.

From PT Require Import
     Monad Relations.
Import RelNotations.

Record stateT (S : Type) (M : Type -> Type) (A : Type) : Type := MkStateT
  { run_stateT : S -> M (S * A)%type }.

Arguments MkStateT {S M A}.
Arguments run_stateT {S M A}.

Section STATE.

Context {S : Type} {M : Type -> Type}.

Instance Monad_stateT `{Monad M} : Monad (stateT S M) :=
  {| ret _ a := MkStateT (fun s => ret (s, a))
   ; subst _ _ k u := MkStateT (fun s =>
       bind (run_stateT u s) (fun sa =>
       run_stateT (k (snd sa)) (fst sa)))
  |}.

Definition rrun_stateT `{RelMonad M} {A : Type}
  : S * stateT S M A -> M (S * A)%type -> Prop :=
  fun st y => rfmap eq (run_stateT (snd st) (fst st)) y.

Definition rret_stateT `{RelMonad M} {A : Type} : A -> stateT S M A -> Prop :=
  fun a u =>
    forall s, rret (s, a) (run_stateT u s).

Definition pair_rel {A B C D : Type}
  : (A -> C -> Prop) ->
    (B -> D -> Prop) ->
    A * B -> C * D -> Prop :=
  fun R1 R2 p q => R1 (fst p) (fst q) /\ R2 (snd p) (snd q).

Definition rover_stateT {A B : Type}
  : (M (S * A)%type -> M (S * B)%type -> Prop) ->
    stateT S M A -> stateT S M B -> Prop :=
  fun r u v =>
    forall s, r (run_stateT u s) (run_stateT v s).

Lemma cat_rover_stateT {A B C : Type}
      (f : M (S * A)%type -> M (S * B)%type -> Prop)
      (g : M (S * B)%type -> M (S * C)%type -> Prop)
  : rover_stateT f >< rover_stateT g <=r rover_stateT (f >< g).
Proof.
  intros ? ? [? []] s; red; eauto.
Qed.

Definition rsubst_stateT `{RelMonad M} {A B : Type}
  : (A -> stateT S M B -> Prop) -> stateT S M A -> stateT S M B -> Prop :=
  fun k =>
    rover_stateT (rsubst (cat_rel (pair_rel eq k) rrun_stateT)).

Instance RelMonad_stateT `{RelMonad M} : RelMonad (stateT S M) :=
  {| rret _ := rret_stateT
   ; rsubst _ _ := rsubst_stateT
  |}.

Global Instance Proper_eq_rel {A B C D : Type}
  : Proper (eq_rel ==> eq_rel ==> eq_rel)
           (@pair_rel A B C D).
Proof. firstorder. Qed.

Global Instance Proper_rover_stateT {A B : Type}
  : Proper (eq_rel ==> eq_rel)
           (rover_stateT (A := A) (B := B)).
Proof. firstorder. Qed.

Local Instance Proper_rsubst_stateT `{RelMonadLaws M} (A B : Type)
  : Proper (eq_rel ==> eq_rel)
           (rsubst (M := stateT S M) (A := A) (B := B)).
Proof.
  intros ? ? E. cbn; unfold rsubst_stateT.
  rewrite E.
  reflexivity.
Qed.

Instance RelMonadLaws_stateT `{RelMonadLaws M} : W.RelMonadLaws (stateT S M).
Proof.
  constructor; [ solve [apply Proper_rsubst_stateT] | .. ];
    intros; cbn; intros; unfold rsubst_stateT.
  - rewrite cat_eq_l. reflexivity.
  - admit.
  - rewrite cat_rover_stateT.
    rewrite rsubst_rsubst.
    rewrite cat_cat.


(*  NNOOOOOOOOOOOOOOOO
  - maybe there's a better definition of rsubst
  - maybe there's a better definition of cat_rel
  - maybe directly define subst for PredT (stateT S M)
 *)

(*
    assert (pair_rel eq f >< rrun_stateT >< rsubst (pair_rel eq g >< rrun_stateT) =r pair_rel eq (f >< rover_stateT (rsubst (pair_rel eq g >< rrun_stateT))) ><
        rrun_stateT).
    { split; intros.
      destruct H0 as [ ? [ [] [? []] ] ].
      hnf.
*)
Abort.

End STATE_.
