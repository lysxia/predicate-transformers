From Coq Require Import
     Setoid Morphisms.

From PT Require Import
     Monad Relations.
Import RelNotations.

Record optionT (M : Type -> Type) (A : Type) : Type := MkOptionT
  { run_optionT : M (option A) }.

Arguments MkOptionT {M A}.
Arguments run_optionT {M A}.

Section OPTION.

Context {M : Type -> Type}.

Instance Monad_stateT `{Monad M} : Monad (optionT M) :=
  {| ret _ a := MkOptionT (ret (Some a))
   ; subst _ _ k u := MkOptionT (
       bind (run_optionT u) (fun oa =>
       match oa with
       | None => ret None
       | Some a => run_optionT (k a)
       end))
  |}.

Definition rrun_optionT {A : Type}
  : optionT M A -> M (option A) -> Prop :=
  fun_rel run_optionT.

Definition rMkOptionT {A : Type}
  : M (option A) -> optionT M A -> Prop :=
  fun_rel MkOptionT.

Definition rret_optionT `{RelMonad M} {A : Type} : A -> optionT M A -> Prop :=
  fun_rel Some >< rret >< rMkOptionT.

Definition rover_optionT {A B : Type}
  : (M (option A) -> M (option B) -> Prop) ->
    optionT M A -> optionT M B -> Prop :=
  fun r u v =>
    r (run_optionT u) (run_optionT v).

Lemma cat_rover_optionT {A B C : Type}
      (f : M (option A)%type -> M (option B)%type -> Prop)
      (g : M (option B)%type -> M (option C)%type -> Prop)
  : rover_optionT f >< rover_optionT g =r rover_optionT (f >< g).
Proof.
  cbv. firstorder. exists (MkOptionT x). auto.
Qed.

Definition rsubst_option {A B : Type}
  : (A -> option B -> Prop) -> option A -> option B -> Prop :=
  fun k oa ob =>
    match oa, ob with
    | None, None => True
    | None, Some _ => False
    | Some a, _ => k a ob
    end.

Definition rconst {A B : Type} (P : B -> Prop)
  : A -> B -> Prop :=
  fun _ => P.

Definition cat_fun {A B C : Type} (f : A -> B) (g : B -> C -> Prop)
  : A -> C -> Prop :=
  fun a => g (f a).

Lemma fun_cat_cat_fun {A B C} (f : A -> B) (g : B -> C -> Prop)
  : fun_rel f >< g =r cat_fun f g.
Proof.
  intros a c. split.
  - intros [ ? [ [] ]]. auto.
  - firstorder.
Qed.

Lemma rconst_eq_cat_fun {A B C} (x : B) (g : B -> C -> Prop)
  : rconst (A := A) (eq x) >< g =r cat_fun (fun _ => x) g.
Proof.
  intros a c. split.
  - intros [ ? [ [] ]]. auto.
  - firstorder.
Qed.

Lemma rmatch_option {A B : Type} (f g : option A -> B -> Prop)
  : rconst (A := True) (eq None) >< f =r rconst (eq None) >< g ->
    fun_rel Some >< f =r fun_rel Some >< g ->
    f =r g.
Proof.
  intros HNone HSome [a |] b.
  - rewrite 2 fun_cat_cat_fun in HSome. apply HSome.
  - rewrite 2 rconst_eq_cat_fun in HNone. apply (HNone I).
Qed.

Definition rsubst_optionT' `{RelMonad M} {A B : Type}
  : (A -> M (option B) -> Prop) -> option A -> M (option B) -> Prop :=
  fun k oa =>
    match oa with
    | None => rret None
    | Some a => k a
    end.

Definition rsubst_optionT `{RelMonad M} {A B : Type}
  : (A -> optionT M B -> Prop) -> optionT M A -> optionT M B -> Prop :=
  fun k =>
    rover_optionT (rsubst (rsubst_optionT' (k >< rrun_optionT))).

Instance RelMonad_optionT `{RelMonad M} : RelMonad (optionT M) :=
  {| rret _ := rret_optionT
   ; rsubst _ _ := rsubst_optionT
  |}.

Lemma rconst_None_rsubst' `{RelMonad M} {A B} (f : A -> M (option B) -> Prop)
  : rconst (A := True) (eq None) >< rsubst_optionT' f
  =r rconst (eq None) >< rret.
Proof.
  rewrite 2 rconst_eq_cat_fun.
  reflexivity.
Qed.

Lemma fun_rel_Some_rsubst' `{RelMonad M} {A B} (f : A -> M (option B) -> Prop)
  : fun_rel Some >< rsubst_optionT' f =r f.
Proof.
  rewrite fun_cat_cat_fun.
  reflexivity.
Qed.

Global Instance Proper_rover_optionT {A B : Type}
  : Proper (eq_rel ==> eq_rel)
           (rover_optionT (A := A) (B := B)).
Proof. firstorder. Qed.

Global Instance Proper_rsubst_optionT' `{RelMonadLaws M} {A B}
  : Proper (eq_rel ==> eq_rel)
           (rsubst_optionT' (A := A) (B := B)).
Proof.
  repeat intro. unfold rsubst_optionT'. destruct a. auto. reflexivity.
Qed.

Local Instance Proper_rsubst_optionT `{RelMonadLaws M} (A B : Type)
  : Proper (eq_rel ==> eq_rel)
           (rsubst (M := optionT M) (A := A) (B := B)).
Proof.
  intros ? ? E. cbn; unfold rsubst_optionT.
Admitted.

Instance RelMonadLaws_optionT `{RelMonadLaws M} : W.RelMonadLaws (optionT M).
Proof.
  constructor; [ solve [apply Proper_rsubst_optionT] | .. ];
    intros; cbn; intros; unfold rsubst_optionT.
  - rewrite cat_eq_l. reflexivity.
  - rewrite cat_eq_l.
  - rewrite cat_rover_optionT.
    rewrite rsubst_rsubst.
    rewrite cat_cat.

    assert (E0
      : forall (f' : A -> M (option B) -> Prop) (g' : B -> M (option C) -> Prop),
         rsubst_optionT' f' >< rsubst (rsubst_optionT' g')
      =r rsubst_optionT' (f' >< rsubst (rsubst_optionT' g'))).
    { intros. apply rmatch_option.
      - rewrite <- cat_cat, 2 rconst_None_rsubst'.
        rewrite cat_cat.
        rewrite rret_rsubst.
        rewrite rconst_None_rsubst'.
        reflexivity.
      - rewrite <- cat_cat, 2 fun_rel_Some_rsubst'.
        reflexivity.
    }

    assert (E1
      : forall (f' : M (option B) -> M (option C) -> Prop),
         rrun_optionT >< f' =r rover_optionT f' >< rrun_optionT).
    { admit. }
    rewrite E0, cat_cat, E1.
    reflexivity.
Qed.

End OPTION_.
