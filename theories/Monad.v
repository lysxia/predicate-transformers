From Coq Require Import
  Setoid Relations Morphisms.

From PT Require Import
  Relations.

Import RelNotations.

(* Monads on functions *)

Class Monad (M : Type -> Type) : Type :=
  { ret : forall A, A -> M A
  ; subst : forall A B, (A -> M B) -> M A -> M B
  }.

Arguments ret {M _ A} : simpl never.
Arguments subst {M _ A B} : simpl never.

Definition bind {M} `{Monad M} {A B} (u : M A) (k : A -> M B) : M B :=
  subst k u.

Definition fmap {M} `{Monad M} {A B} (f : A -> B) : M A -> M B :=
  subst (fun a => ret (f a)).

(* Monads on relations *)

Class RelMonad (M : Type -> Type) : Type :=
  { rret : forall A, A -> M A -> Prop
  ; rsubst : forall A B, (A -> M B -> Prop) -> M A -> M B -> Prop
  }.

Arguments rret {M _ A}.
Arguments rsubst {M _ A B}.

Definition rbind {M} `{RelMonad M} {A B} (u : M A) (k : A -> M B -> Prop) : M B -> Prop :=
  rsubst k u.

Definition rfmap {M} `{RelMonad M} {A B} (u : A -> B -> Prop) : M A -> M B -> Prop :=
  rsubst (u >< rret).

Class RelMonadLaws (M : Type -> Type) {RM : RelMonad M} : Prop :=
  { Proper_rsubst :> forall A B, Proper (eq_rel ==> eq_rel) (rsubst (A := A) (B := B))
  ; rsubst_rret : forall A,
      rsubst rret =r @eq (M A)
  ; rret_rsubst : forall A B (f : A -> M B -> Prop),
      rret >< rsubst f =r f
  ; rsubst_rsubst : forall A B C (f : A -> M B -> Prop) (g : B -> M C -> Prop),
      rsubst f >< rsubst g =r rsubst (f >< rsubst g)
  }.

Module W. (* Weak laws *)

Class RelMonadLaws (M : Type -> Type) {RM : RelMonad M} : Prop :=
  { Proper_rsubst : forall A B, Proper (eq_rel ==> eq_rel) (rsubst (A := A) (B := B))
  ; rsubst_rret : forall A,
      rsubst rret =r rfmap (@eq A)
  ; rret_rsubst : forall A B (f : A -> M B -> Prop),
      rret >< rsubst f =r f >< rfmap eq
  ; rsubst_rsubst : forall A B C (f : A -> M B -> Prop) (g : B -> M C -> Prop),
      rsubst f >< rsubst g =r rsubst (f >< rsubst g)
  }.

End W.

(* [fun_rel] is a monad morphism between [M] as a monad on functions
   and [M] as a monad on relations. *)
Class RelMonadMorphism (M : Type -> Type) {MM : Monad M} {RM : RelMonad M} : Prop :=
  { rret_fun_rel_ret : forall A, rret (A := A) =r fun_rel ret
  ; rsubst_fun_rel_subst : forall A B (k : A -> M B),
      rsubst (fun_rel k) =r fun_rel (subst k)
  }.

Section RMM.

Context
  {M : Type -> Type}
  {MM : Monad M}
  {RM : RelMonad M}
  {RMM : RelMonadMorphism M}.

Lemma rret_ret {A} (a : A) : rret a (ret a).
Proof.
  apply rret_fun_rel_ret; reflexivity.
Qed.

Lemma rsubst_subst {A B} (u : M A) (k : A -> M B)
  : rsubst (fun_rel k) u (subst k u).
Proof.
  apply rsubst_fun_rel_subst; reflexivity.
Qed.

End RMM.

(* Option monad (partiality, exceptions) *)

Definition subst_option {A B : Type} (k : A -> option B) (o : option A) : option B :=
  match o with
  | None => None
  | Some a => k a
  end.

Instance Monad_option : Monad option :=
  {| ret := @Some
   ; subst := @subst_option
  |}.

Instance RelMonad_option : RelMonad option.
Admitted.

Instance RelMonadLaws_option : RelMonadLaws option.
Admitted.

Instance RelMonadMorphism_option : RelMonadMorphism option.
Admitted.
