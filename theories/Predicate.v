(* Predicate monad transformer *)

From Coq Require Import
  Setoid Relations Morphisms.
From PT Require Import
  Relations Monad.

Import RelNotations.

Section Pred.

Context
  {M : Type -> Type}
  {Monad_M : RelMonad M}.

Definition lift_pred {A} : M A -> M A -> Prop := eq.

Definition subst_pred {A B} (Pk : A -> M B -> Prop) (Pu : M A -> Prop) : M B -> Prop :=
  rsubst Pk <| Pu.

Definition ret_pred {A} : A -> M A -> Prop := rret.

End Pred.

Section PredProof.

Notation lift := lift_pred.

Context
  {M : Type -> Type}
  {MM : Monad M}
  {RM : RelMonad M}
  {RML : RelMonadLaws M}
  {RMM : RelMonadMorphism M}.

Global Instance Proper_subst_pred {A B}
  : Proper (eq_rel ==> eq_pred ==> eq_pred)
           (@subst_pred M _ A B).
Proof.
Admitted.

(* The predicate monad transformer [M _ -> Prop] defines a monad. *)

Theorem subst_ret_r {A B} (a : A) (pk : A -> M B -> Prop)
  : eq_pred (subst_pred pk (ret_pred a))
            (pk a).
Proof.
  unfold subst_pred, ret_pred.
  rewrite rmap_rel.
  apply eq_rel_pred.
  rewrite rret_rsubst.
  reflexivity.
Qed.

Theorem subst_ret_l {A} (pu : M A -> Prop)
  : eq_pred (subst_pred ret_pred pu)
            pu.
Proof.
  unfold subst_pred, ret_pred.
  rewrite rsubst_rret.
  apply rmap_pred_eq.
Qed.

Theorem subst_subst {A B C} (pu : M A -> Prop) (pk : A -> M B -> Prop)
        (ph : B -> M C -> Prop)
  : eq_pred (subst_pred ph (subst_pred pk pu))
            (subst_pred (fun a => subst_pred ph (pk a)) pu).
Proof.
  unfold subst_pred, ret_pred.
  rewrite rmap_rmap.
  apply Proper_rmap_pred; try reflexivity.
  rewrite rsubst_rsubst.
  apply Proper_rsubst.
  rewrite compose_rmap.
  reflexivity.
Qed.

(* It is a monad morphism. *)

Theorem lift_subst {A B} (u : M A) (k : A -> M B)
  : eq_pred (lift (subst k u))
            (subst_pred (fun a => lift (k a)) (lift u)).
Proof.
  unfold subst_pred, lift.
  rewrite rmap_rel.
  rewrite eq_pred_fun_rel.
  apply eq_rel_pred.
  rewrite cat_eq_l.
  rewrite <- rsubst_fun_rel_subst.
  reflexivity.
Qed.

End PredProof.
